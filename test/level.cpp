#include <logging/level.hpp>

namespace jaf::logging::test
{
    struct level
        : ::testing::TestWithParam<
            std::pair<::jaf::logging::level,std::string>
        >
    {
    };

    TEST_F(level, read_invalid)
    {
        auto ss = std::stringstream{ "invalid" };
        auto l = ::jaf::logging::level::info;
        EXPECT_THROW(ss >> l, std::invalid_argument);
    }

    TEST_P(level, print)
    {
        const auto& p = GetParam();

        auto ss = std::stringstream{};
        ss << p.first;

        EXPECT_EQ(ss.str(), p.second);
    }

    TEST_P(level, read)
    {
        const auto& p = GetParam();

        auto ss = std::stringstream{ p.second };
        auto l = ::jaf::logging::level::info;
        ss >> l;

        EXPECT_EQ(l, p.first);
    }

    const std::array<typename level::ParamType, 6> print_values =
    {{
        { ::jaf::logging::level::trace, "trace" },
        { ::jaf::logging::level::debug, "debug" },
        { ::jaf::logging::level::info, "info" },
        { ::jaf::logging::level::error, "error" },
        { ::jaf::logging::level::fatal, "fatal" },
        { ::jaf::logging::level::none, "none" }
    }};

    INSTANTIATE_TEST_SUITE_P(, level, ::testing::ValuesIn(print_values));
}
