#include <logging/base.hpp>

namespace jaf::logging::test
{
    struct base
        : ::testing::Test
    {
    };

    struct mock_streambuf
        : std::streambuf
    {
        MOCK_METHOD(std::streamsize, xsputn, (const char*, std::streamsize));
    };

    struct matcher
    {
        matcher(std::string exp)
            : exp_{ std::move(exp) }
        {
        }

        std::streamsize operator()(const char* b, std::streamsize c)
        {
            EXPECT_EQ(exp_, b);
            return c;
        }

    private:
        std::string exp_;
    };

    TEST_F(base, log_higher)
    {
        using ::testing::Invoke;

        auto mock = mock_streambuf{};
        auto stream = std::ostream{ &mock };

        EXPECT_CALL(mock, xsputn)
            .Times(4)
            .WillOnce(Invoke(matcher{"["}))
            .WillOnce(Invoke(matcher{"fatal"}))
            .WillOnce(Invoke(matcher{"]"}))
            .WillOnce(Invoke(matcher{"hi"}));

        auto log = ::jaf::logging::base{ ::jaf::logging::level::info, stream };

        log.fatal() << "hi";
    }
    
    TEST_F(base, log_equal)
    {
        using ::testing::Invoke;

        auto mock = mock_streambuf{};
        auto stream = std::ostream{ &mock };

        EXPECT_CALL(mock, xsputn)
            .Times(4)
            .WillOnce(Invoke(matcher{"["}))
            .WillOnce(Invoke(matcher{"debug"}))
            .WillOnce(Invoke(matcher{"]"}))
            .WillOnce(Invoke(matcher{"test"}));

        auto log = ::jaf::logging::base{ ::jaf::logging::level::debug, stream };

        log.debug() << "test";
    }

    TEST_F(base, dont_log_lower)
    {
        auto mock = mock_streambuf{};
        auto stream = std::ostream{ &mock };

        EXPECT_CALL(mock, xsputn)
            .Times(0);

        auto log = ::jaf::logging::base{ ::jaf::logging::level::fatal, stream };

        log.info() << "hi";
    }

    TEST_F(base, same_ostream)
    {
        auto mock = mock_streambuf{};
        auto stream = std::ostream{ &mock };

        auto log = ::jaf::logging::base{ ::jaf::logging::level::trace, stream };
        auto& ls = log.info();

        EXPECT_EQ(std::addressof(stream), std::addressof(ls));
    }
}
