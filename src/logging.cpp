#include <logging/logging.hpp>

namespace jaf::logging
{
    namespace details
    {
        struct stream_holder
        {
            std::ostream* stream = nullptr;
        };

        stream_holder& singleton()
        {
            static auto s = stream_holder{};
            return s;
        }

        std::ostream& get_stream()
        {
            return *(singleton().stream);
        }
    }

    void setup(std::ostream& s)
    {
        details::singleton().stream = std::addressof(s);
    }

    void teardown()
    {
        details::singleton().stream = nullptr;
    }

}