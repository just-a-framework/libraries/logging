#pragma once

#include <logging/logger.hpp>

#include <ostream>

namespace jaf::logging
{
    struct base
        : logger
    {
        base(level l, std::ostream& out);

        std::ostream& fatal() final;
        std::ostream& error() final;
        std::ostream& info() final;
        std::ostream& debug() final;
        std::ostream& trace() final;
    
    protected:
        virtual std::ostream& init(std::ostream& os);

    private:
        struct null_buffer
            : std::streambuf
        {
            int overflow(int c);
        };

        std::ostream& log(level l);
        static std::ostream& null();

        std::ostream& out_;
        level l_;
    };
}
