#pragma once

#include <logging/level.hpp>

#include <ostream>

namespace jaf::logging
{
    struct logger
    {
        virtual std::ostream& fatal() = 0;
        virtual std::ostream& error() = 0;
        virtual std::ostream& info() = 0;
        virtual std::ostream& debug() = 0;
        virtual std::ostream& trace() = 0;

        virtual ~logger();
    };
}
