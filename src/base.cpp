#include <logging/base.hpp>

#include <iostream>

namespace jaf::logging
{
    base::base(level l, std::ostream& out)
        : l_{ std::move(l) }
        , out_{ out }
    {
    }

    std::ostream& base::fatal()
    {
        return log(level::fatal);
    }

    std::ostream& base::error()
    {
        return log(level::error);
    }

    std::ostream& base::info()
    {
        return log(level::info);
    }

    std::ostream& base::debug()
    {
        return log(level::debug);
    }

    std::ostream& base::trace()
    {
        return log(level::trace);
    }

    std::ostream& base::init(std::ostream& os)
    {
        return os;
    }

    std::ostream& base::log(level l)
    {
        auto& logger_ = (l <= l_ ? out_ : null());

        logger_ << "[" << l << "]";

        return init(logger_);
    }

    std::ostream& base::null()
    {
        static null_buffer buff;
        static std::ostream null{ &buff };
        return null;
    }

    int base::null_buffer::overflow(int c)
    {
        return c;
    }
}
