from conans import ConanFile, CMake

class JafLoggingPort(ConanFile):
    name = "jaf_logging"
    version = "0.0.1"
    description = "Another c++ logging library"
    url = "https://gitlab.com/just-a-framework/libraries/logging"
    settings = "os", "compiler", "build_type", "arch"
    generators = "cmake"
    exports_sources = "src*"
    requires = "jaf_core/0.0.1@just-a-framework+libraries+core/testing"
    build_requires = "cmake/3.19.5"

    def _configure_cmake(self):
        cmake = CMake(self)
        cmake.definitions["CONAN_BUILD"] = "ON"
        cmake.configure(source_folder="src")
        return cmake

    def build(self):
        cmake = self._configure_cmake()
        cmake.build()

    def package(self):
        cmake = self._configure_cmake()
        cmake.install()
    
    def package_info(self):
        self.cpp_info.builddirs = ["lib/jaf/cmake"]
